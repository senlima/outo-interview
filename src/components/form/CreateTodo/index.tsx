import React from 'react'
import { useForm } from 'react-hook-form'

import { useAppDispatch } from 'store'
import { addTodo } from 'store/todos'
import styles from './index.module.css'

type FormType = {
  memo: string
}

export function CreateTodoForm() {
  const dispatch = useAppDispatch()
  const { register, handleSubmit, reset } = useForm<FormType>()

  function onSubmit(data: FormType) {
    dispatch(addTodo(data))
    reset()
  }

  return (
    <form className={styles.container} onSubmit={handleSubmit(onSubmit)}>
      <input
        className={styles.memoInput}
        name="memo"
        ref={register({ required: true, minLength: 1 })}
        aria-label="memo-input"
      />
      <button
        className={styles.memoSubmit}
        type="submit"
        aria-label="memo-submit"
      >
        submit
      </button>
    </form>
  )
}

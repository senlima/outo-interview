import React from 'react'
import { render, fireEvent, waitFor } from 'test-utils'
import * as actions from 'store/todos/actions'

import { CreateTodoForm } from '.'

const setup = () => {
  const utils = render(<CreateTodoForm />, { initialState: { todos: [] } })
  const input = utils.getByLabelText('memo-input') as HTMLInputElement
  const submit = utils.getByLabelText('memo-submit') as HTMLButtonElement
  return {
    input,
    submit,
    ...utils,
  }
}

const click = { button: 1 }

describe('<CreateTodoForm />', () => {
  it('should render', () => {
    const { input, submit } = setup()

    expect(input.value).toBe('')
    expect(submit).toBeInTheDocument()
  })

  it('should handle onSubmit', () => {
    const spyAction = jest.spyOn(actions, 'addTodo')
    const { input, submit } = setup()
    fireEvent.input(input, {
      target: { value: 'test form' },
    })
    fireEvent.click(submit, click)

    waitFor(() => {
      expect(input.value).toBe('')
      expect(spyAction).toBeCalled()
    })
  })
})

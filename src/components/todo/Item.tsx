import React, { FC } from 'react'
import { useAppDispatch } from 'store'

import { removeTodo } from 'store/todos'
import { TodoItem as TodoItemType } from '@types'
import styles from './Item.module.css'

type Props = {
  item: TodoItemType
}

export const TodoItem: FC<Props> = ({ item }) => {
  const dispatch = useAppDispatch()
  const date = new Date(item.createdAt)

  return (
    <li className={styles.container} data-testid="todo-item">
      <button
        className={styles.closeBtn}
        type="button"
        aria-label="delete-button"
        onClick={() => dispatch(removeTodo(item.id))}
      >
        x
      </button>
      <p style={{ marginTop: 0 }}>{item.memo}</p>
      {item.address && <address>{item.address}</address>}
      <time>{new Intl.DateTimeFormat().format(date)}</time>
    </li>
  )
}

import React from 'react'
import { render, fireEvent } from 'test-utils'
import * as actions from 'store/todos/actions'

import { TodoItem } from './Item'

const testItem = {
  id: 'test-uuid',
  memo: 'Test that slice',
  address: 'home',
  createdAt: 1605310730803,
}

const setup = () => {
  const utils = render(<TodoItem item={testItem} />, {
    initialState: { todos: [testItem] },
  })
  const title = utils.getByText('Test that slice') as HTMLParagraphElement
  const address = utils.getByText('home') as HTMLSpanElement
  const createdAt = utils.getByText('2020-11-14') as HTMLTimeElement
  const deleteBtn = utils.getByLabelText('delete-button') as HTMLButtonElement

  return {
    title,
    address,
    createdAt,
    deleteBtn,
    ...utils,
  }
}

describe('<TodoItem />', () => {
  it('should render props', () => {
    const { title, address, createdAt, deleteBtn } = setup()

    expect(title).toBeInTheDocument()
    expect(address).toBeInTheDocument()
    expect(createdAt).toBeInTheDocument()
    expect(deleteBtn).toBeInTheDocument()
  })

  it('should dispatch remove when button clicked', () => {
    const spyAction = jest.spyOn(actions, 'removeTodo')

    const { deleteBtn } = setup()
    fireEvent.click(deleteBtn)

    expect(spyAction).toHaveBeenCalled()
  })
})

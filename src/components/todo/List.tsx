import React from 'react'
import { useSelector } from 'react-redux'

import { selectTodo } from 'store'
import { TodoItem } from './Item'
import styles from './List.module.css'

export const TodoList = () => {
  const todos = useSelector(selectTodo)

  if (todos.length === 0) {
    return <p>Go to add memo</p>
  }

  return (
    <ul className={styles.list} data-testid="todo-list">
      {todos.map(item => (
        <TodoItem key={item.id} item={item} />
      ))}
    </ul>
  )
}

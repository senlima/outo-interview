import React from 'react'
import { render } from 'test-utils'

import { TodoList } from './List'
import { TodoItem } from '@types'

const testItem = {
  id: 'test-uuid',
  memo: 'Test that slice',
  address: 'home',
  createdAt: 1605310730803,
}

const setup = (init: TodoItem[] = []) => {
  const utils = render(<TodoList />, {
    initialState: { todos: init },
  })
  return {
    ...utils,
  }
}

describe('<TodoList />', () => {
  it('should handle empty', () => {
    const { getByText } = setup()
    const emptyHint = getByText('Go to add memo') as HTMLParagraphElement

    expect(emptyHint).toBeInTheDocument()
  })

  it('should render list', () => {
    const { getByTestId, getAllByTestId } = setup([testItem])

    const list = getByTestId('todo-list') as HTMLUListElement
    const items = getAllByTestId('todo-item') as HTMLLIElement[]

    expect(list).toBeInTheDocument()
    expect(items.length).toBe(1)
  })
})

import React from 'react'
import { render } from 'test-utils'

import App from './App'

const setup = () => {
  const utils = render(<App />, {
    initialState: { todos: [] },
  })
  const app = utils.getByTestId('todo-app') as HTMLDivElement
  return {
    app,
    ...utils,
  }
}

describe('<App />', () => {
  it('should render', () => {
    const { app } = setup()

    expect(app).toBeInTheDocument()
  })
})

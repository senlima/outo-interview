import { useDispatch } from 'react-redux'
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'

import rootReducer from './reducers'

const middleware = [...getDefaultMiddleware()]

const store = configureStore({
  reducer: rootReducer,
  middleware,
})

export type AppDispatch = typeof store.dispatch
export const useAppDispatch = () => useDispatch<AppDispatch>()

export type RootState = ReturnType<typeof rootReducer>
export const selectTodo = (state: RootState) => state.todos

export default store

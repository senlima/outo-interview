import { createSlice } from '@reduxjs/toolkit'

import { initialState } from './initialState'
import { createReducer, insertTodo, addTodo, removeTodo } from './actions'

const todosSlice = createSlice({
  name: 'todos',
  initialState,
  reducers: {},
  extraReducers: builder => createReducer(builder),
})

export { insertTodo, addTodo, removeTodo }

export default todosSlice.reducer

import crs from 'crypto-random-string'
import todosReducer, { addTodo, removeTodo, insertTodo } from '.'
import { addTodoPrepare } from './actions'

jest.mock('crypto-random-string')

const testItem = {
  id: 'test-uuid',
  memo: 'Test that slice',
  createdAt: 1605310730803,
}

describe('todo slice', () => {
  it('insertTodo', () => {
    expect(
      todosReducer([], {
        type: insertTodo.type,
        payload: [testItem, testItem],
      })
    ).toEqual([testItem, testItem])
  })

  it('removeTodo', () => {
    expect(
      todosReducer([testItem], {
        type: removeTodo.type,
        payload: {
          id: testItem.id,
        },
      })
    ).toEqual([])
  })

  it('addTodoPrepare', () => {
    // @ts-ignore
    crs.mockReturnValueOnce('tests')
    const data = addTodoPrepare({ memo: 'Test' })

    expect(data.payload.id.length).toBe(5)
    expect(data.payload.memo).toBe('Test')
    expect(crs).toBeCalledWith({ length: 5 })
  })

  it('addTodo', () => {
    expect(
      todosReducer([], {
        type: addTodo.type,
        payload: {
          memo: 'Test that slice',
        },
      }).length
    ).toBe(1)
  })
})

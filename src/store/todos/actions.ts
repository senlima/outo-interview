import {
  createAction,
  PrepareAction,
  ActionReducerMapBuilder,
} from '@reduxjs/toolkit'
import crs from 'crypto-random-string'

import { TodoItem } from '@types'

export const addTodoPrepare = ({ memo }: { memo: string }) => {
  return {
    payload: {
      id: crs({ length: 5 }),
      memo,
      createdAt: new Date().getTime(),
    },
  }
}

export const removeTodo = createAction<string, 'remove'>('remove')
export const insertTodo = createAction<TodoItem[], 'insert'>('insert')
export const addTodo = createAction<PrepareAction<TodoItem>, 'create'>(
  'create',
  addTodoPrepare
)

export const createReducer = (
  builder: ActionReducerMapBuilder<TodoItem[]>
) => {
  builder
    .addCase(insertTodo, (state, { payload }) => {
      payload.map(item => state.push(item))
    })
    .addCase(addTodo, (state, { payload }) => {
      state.push(payload)
    })
    .addCase(removeTodo, (state, { payload }) => {
      const target = state.findIndex(item => item.id === payload)
      state.splice(target, 1)
    })
}

export type TodoItem = {
  id: string
  memo: string
  address?: string
  createdAt: number
}

export type TodoInput = {
  memo: string
  createdAt: number
}

export type TodoCreatePayload = {
  id: string
  memo: string
  address?: string
  createdAt: number
}

import React, { useEffect } from 'react'

import styles from './App.module.css'
import { TodoList } from 'components/todo/List'
import { CreateTodoForm } from 'components/form/CreateTodo'
import { useAppDispatch } from 'store'
import { insertTodo } from 'store/todos'
import response from 'response.json'

function App() {
  const dispatch = useAppDispatch()

  useEffect(() => {
    dispatch(insertTodo(response))
  }, [dispatch])

  return (
    <div className={styles.container} data-testid="todo-app">
      <TodoList />
      <CreateTodoForm />
    </div>
  )
}

export default App

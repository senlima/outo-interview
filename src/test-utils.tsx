import React, { FC, ReactElement } from 'react'
import { render as rtlRender } from '@testing-library/react'
import { createStore, Store, CombinedState, AnyAction } from 'redux'
import { Provider } from 'react-redux'

import { TodoItem } from '@types'
import { RootState } from 'store'
import rootReducer from 'store/reducers'

interface Field {
  initialState: RootState
  store?: Store<CombinedState<{ todos: TodoItem[] }>, AnyAction>
}

function render(
  ui: ReactElement,
  {
    initialState,
    store = createStore(rootReducer, initialState),
    ...renderOptions
  }: Field
) {
  const Wrapper: FC = ({ children }) => (
    <Provider store={store}>{children}</Provider>
  )
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
}

export * from '@testing-library/react'

export { render }
